# -*- coding: utf-8 -*-
import click, clack, logging
from click._compat import PY2


def test_catch_exception(runner, caplog):
    @click.command()
    @clack.safe_execute()
    def cmd():
        raise Exception('Hello')

    result = runner.invoke(cmd)
    assert not result.exception
    assert ["Hello\nTRACEBACK    File /development/wmuk/clack/tests/test_safe_execute.py, line 10, in cmd\nTRACEBACK      raise Exception('Hello')"] == [rec.message for rec in caplog.records]

def test_catch_exception_verbose(runner, caplog):
    @click.command()
    @clack.safe_execute()
    def cmd():
        raise Exception('Hello')

    result = runner.invoke(cmd, ['--verbose'])
    assert ["Hello\nTRACEBACK    File /development/wmuk/clack/tests/test_safe_execute.py, line 20, in cmd\nTRACEBACK      raise Exception('Hello')"] == [rec.message for rec in caplog.records]
    assert result.output.splitlines() == [u'  File "clack/decorators.py", line 26, in catch_exceptions',
              u'    return fn(*args, **kwargs)',
              u'  File "/development/wmuk/clack/tests/test_safe_execute.py", line 20, in cmd',
              u'    raise Exception(\'Hello\')']

def test_no_exception(runner):
    @click.command()
    @clack.safe_execute()
    def cmd():
        click.echo('test_no_exception:yes')

    result = runner.invoke(cmd)
    assert not result.exception
    assert result.output == 'test_no_exception:yes\n'
