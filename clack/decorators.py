import sys, traceback, logging
from functools import update_wrapper
from click import decorators

def safe_execute(log_format="{0}\nTRACEBACK    File {1[0]}, line {1[1]}, in {1[2]}\nTRACEBACK      {1[3]}", pass_flags=False):
    """Wraps the executed function in an exception handler that will
    log errors and then cleanly execute. It also installs an Option
    call `verbose` which will print a full traceback on error.

    The verbose Option will *not* be passed to the called function
    because safe_execute will suppress it. This means if you manually
    try to create a verbose option you will lose that too. To pass the
    Option set the `pass_flags` paramter to this decorator to `True`.

    :param log_format: How to format the log entry
    :param pass_flags: if set to `True` will pass the verbose Option to the called function
    """
    def setup_safe_execute(fn):
        #
        #decorators._param_memo(fn, OptionClass('--verbose', is_flag=True))
        def catch_exceptions(*args, **kwargs):
            verbose = 'verbose' in kwargs and kwargs['verbose']
            try:
                if not pass_flags:
                    del(kwargs['verbose'])
                return fn(*args, **kwargs)
            except Exception as err:
                line = traceback.extract_tb(sys.exc_info()[2]).pop()
                log_entry = log_format.format(str(err), line)
                if verbose:
                    logging.error(log_entry)
                    traceback.print_tb(sys.exc_info()[2])
                    sys.exit(1)
                else:
                    logging.critical(log_entry)
        return decorators.option('--verbose/-v')(update_wrapper(catch_exceptions,fn))
    return setup_safe_execute
